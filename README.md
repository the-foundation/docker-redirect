# Docker-redirect

Docker redirect template based on schmunk42/nginx-redirect


> ### Parameters (in .env)
> ```
> HOSTNAME=seconddomain.com
> TARGET=myrealdomain.com
> LETSENCRYPT_EMAIL=default-mail-not-set@using-fallback-default.slmail.me
> ```

> ### optional
> ```
> NGINX_NETWORK=docker-frontent-proxy-network
> SERVER_REDIRECT_PATH=/my/subdirectory/to/redirect/to
> ```

---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-redirect/README.md/logo.jpg" width="480" height="270"/></div></a>
